import * as React from "react";

const Footer = () => {
  return (
    <div className="mt-5">
      <p>&copy; 2021 - By Marouéne TAYARI - All rights reserved</p>
    </div>
  );
};

export default Footer;
