import Axios from "axios";

export const getProjectList = async () => {
  return await Axios.get("/api/projects").then(
    (res) => {
      return res.data;
    }
  );
};

/**
 * storeNewProject()
 *
 * @param {object} data
 */
export const storeNewProject = async (data) => {
  data.user_id = 1;
  return await Axios.post(
    "/api/projects",
    data
  ).then((res) => {
    return res.data;
  });
};

/**
 * Update Project
 * @param id
 * @param data
 * @returns {Promise<AxiosResponse<T>>}
 */
export const updateProject = async (id, data) => {
  data.user_id = 1;
  return await Axios.put(
    `/api/projects/${id}`,
    data
  ).then((res) => {
    return res.data;
  });
};

/**
 * Delete project
 * @param id
 * @returns {Promise<AxiosResponse<T>>}
 */
export const deleteProject = async (id) => {
  return await Axios.delete(
    `/api/projects/${id}`
  ).then((res) => {
    return res.data;
  });
};
