# ToDo Application - (Laravel et React) [Demo](https://to-do-app.twin-dev.com/)

Application de gestion des tâches de base utilisant Laravel et React Framework. Les API sont basées sur Laravel PHP Framework 7.12 et Frontend Developed basé sur React JS.
Ici, l'utilisateur peut s'inscrire et se connecter et peut gérer ses projets et chaque tâche à l'intérieur d'un projet.

## Requirements

1. Composer Install
1. Xampp (PHP > 7.3)
1. Node Js
1. Basic Laravel Project
1. React CLI
1. Laravel V-7.12

## IDE
- PHPStorm 2018.1.2

## Installation 

Voici donc les étapes à réaliser pour installer ce projet.

##### **1-** Ouvrer votre terminal si vous avez un environnement (Linux ou Mac OS) ou bien si vous êtes sur un environnement Windows vous pouvez utiliser GitBash

##### **2-** Cloner le projet via la commande : 
``` 
git clone git@gitlab.com:Mriwech/todo-application-laravel-react.git 
```
##### **3-** Accédez au répertoire de votre projet
``` 
cd todo-application-laravel-react
``` 
##### **5-** Installer les dépendances du projet à partir de composer
``` 
composer install
``` 
##### **6-** Installer les dépendances NPM 
``` 
npm install
```
##### **7-** Créez la base de donnée dans votre environnement    
>**db_task_management**

##### **8-A** Lancer la création des tables de la base des données avec la commande : 
``` 
php artisan migrate
```
##### **8-B** C'est le moment de l’exécuter votre Seeder, afin de remplir votre base de données avec des données de démarrage : 
``` 
php artisan db:seed
```
##### **9-** La création de Client Credentials Grant Tokens avec le nom de 'authToken' a fin d'assurer la bon comunication entre le client et le backoffice: 
``` 
php artisan passport:client --personal
What should we name the client ? 
> authToken
```
##### **10-** Lancer le Webpack avec la commande : 
``` 
npm run watch
```
##### **11-** Lancer votre serveur local via la commande : 
``` 
php artisan serve
```
##### **12-** Ouvrire le project sur votre navigateur :
[http://127.0.0.1:8000/](http://127.0.0.1:8000/)

Have fun. :) 

## Screenshots
![Page inscription](https://to-do-app.twin-dev.com/screenshot/1_Inscription.png) 1- Page inscription

![Page connexion](https://to-do-app.twin-dev.com/screenshot/2_Connexion.png) 2- Page connexion

![Home page](https://to-do-app.twin-dev.com/screenshot/3_Home.png) 3- Page d'accueil

![Liste des projets](https://to-do-app.twin-dev.com/screenshot/4_Liste_Of_Projects.png) 4- La liste des projets

![Creation de projet](https://to-do-app.twin-dev.com/screenshot/5_Create_project.png) 5- La création d'un nouveau projet 

![Détail d'un projet](https://to-do-app.twin-dev.com/screenshot/6_Details_Project.png) 6- Page détail d'un projet 

![Edit d'un projet](https://to-do-app.twin-dev.com/screenshot/7_Edit_Project.png) 7- Page edition projet 

![Ajouter une tâche](https://to-do-app.twin-dev.com/screenshot/8_Add_Task.png) 8- Ajouter une tâche 


## Elaboré par
- [Tayari Marouéne](https://tayari.twin-dev.com) - Développeur FullStack
